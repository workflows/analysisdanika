# analysis base version from which we start
FROM atlas/analysisbase:21.2.51

# put the code into the repo with the proper owner
COPY --chown=atlas . /code/src

# note that the build directory already exists in /code/src
RUN sudo chown -R atlas /code/src && \
    cd /code/src && \
    source /home/atlas/release_setup.sh && \  
    g++ `root-config --glibs --cflags` -o analysis.exe analysis.cpp
    
# where you are left after booting the image
WORKDIR /code/src

# Lines needed to run with reana - from Danika
USER root
RUN sudo usermod -G root atlas
USER atlas
